package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table 
@Entity
@Component
public class Timetable {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column(nullable = false)
	    private String weekday;

	    @Column(nullable = false)
	    private String timeSlot;

	   
       @Column(nullable = false)
       private String Activity;
       
       @Column(nullable = false)
	    private String className; 
	    
	    
	    public Long getId() {
			return id;
		}



		public void setId(Long id) {
			this.id = id;
		}



		public String getWeekday() {
			return weekday;
		}



		public void setWeekday(String weekday) {
			this.weekday = weekday;
		}



		public String getTimeSlot() {
			return timeSlot;
		}



		public void setTimeSlot(String timeSlot) {
			this.timeSlot = timeSlot;
		}



		public String getClassName() {
			return className;
		}



		public void setClassName(String className) {
			this.className = className;
		}



		public String getActivity() {
			return Activity;
		}



		public void setActivity(String activity) {
			Activity = activity;
		}



 
}
