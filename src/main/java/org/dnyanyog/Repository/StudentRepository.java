package org.dnyanyog.Repository;

import org.dnyanyog.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface StudentRepository extends JpaRepository<Student,Long>{

	Student findBystudentMob(String StudentMob);
//	Student findById(String id);
	
}
