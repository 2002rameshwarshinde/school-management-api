package org.dnyanyog.Repository;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.LeaveApplication;

import org.dnyanyog.entity.LeaveStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface LeaveRepository extends JpaRepository<LeaveApplication, Long> {

	

	List<LeaveApplication> findByStatus(LeaveStatus status);

	 List<LeaveApplication> findByStatus(String status);
	 
	 Optional<LeaveApplication> findByIdAndStatus(Long id, LeaveStatus status);
	 
//	 LeaveApplication findByGrnNo(Long grnNo);
	}


