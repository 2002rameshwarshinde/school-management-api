package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class RegistrationRequest {

	private String MobileNo;
	
	private String Password;
	
	private String Role;

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}
	
}
