package org.dnyanyog.service;

import org.dnyanyog.Dto.Request.RegistrationRequest;

import org.dnyanyog.Dto.response.RegistrationResponse;
import org.dnyanyog.Repository.UserRepository;
import org.dnyanyog.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class RegistrationService {

	@Autowired
	RegistrationResponse response;

	@Autowired
	User user;

	

	@Autowired
	UserRepository userRepo;

	public ResponseEntity<RegistrationResponse> saveData(RegistrationRequest request) {

		response = new RegistrationResponse();
		

		
		user = new User();
		
		user.setMobileNo(request.getMobileNo());
		user.setPassword(request.getPassword());
		user.setRole(request.getRole());

		user = userRepo.save(user);
		response.setStatus("Success");
		response.setMassage("SingnUp SucccessFull");

		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

}
