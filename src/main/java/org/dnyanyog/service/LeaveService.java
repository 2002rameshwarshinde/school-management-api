package org.dnyanyog.service;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.Dto.Request.LeaveApplicationRequest;

import org.dnyanyog.Repository.LeaveRepository;
import org.dnyanyog.entity.LeaveApplication;
import org.dnyanyog.entity.LeaveStatus;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class LeaveService {

	@Autowired
	private LeaveRepository leaveRequestRepository;

	@Autowired
	LeaveApplication leaveApplication;

	public LeaveApplication applyforLeave(LeaveApplicationRequest leaveApplicationRequest) {

		leaveApplication.setGrnNO(leaveApplicationRequest.getGrnNo());
		leaveApplication.setStudentName(leaveApplicationRequest.getStudentName());
		leaveApplication.setStartDate(leaveApplicationRequest.getStartDate());
		leaveApplication.setEndDate(leaveApplicationRequest.getEndDate());
		leaveApplication.setReason(leaveApplicationRequest.getReason());
		leaveApplication.setStatus(LeaveStatus.PENDING);
		// leaveApplication.setResponse(null);
		return leaveRequestRepository.save(leaveApplication);

	}

	public List<LeaveApplication> getLeaveRequestsByStatus(String status) {
		LeaveStatus leaveStatus = LeaveStatus.valueOf(status.toUpperCase());

		return leaveRequestRepository.findByStatus(leaveStatus);
	}

//	public LeaveApplication getLeaveRequestByGRN(Long grnNo) {
//		return leaveRequestRepository.findByGrnNo(grnNo);
//	}

	public boolean approveLeaveRequest(Long requestId) {
		Optional<LeaveApplication> optionalLeaveRequest = leaveRequestRepository.findById(requestId);

		if (optionalLeaveRequest.isPresent()) {
			LeaveApplication leaveRequest = optionalLeaveRequest.get();
			leaveRequest.setStatus(LeaveStatus.APPROVED);
			leaveRequestRepository.save(leaveRequest);
			return true;
		} else {
			return false;
		}
	}

	public boolean rejectLeaveRequest(Long requestId) {
		Optional<LeaveApplication> optionalLeaveRequest = leaveRequestRepository.findById(requestId);

		if (optionalLeaveRequest.isPresent()) {
			LeaveApplication leaveRequest = optionalLeaveRequest.get();
			leaveRequest.setStatus(LeaveStatus.REJECTED);
			leaveRequestRepository.save(leaveRequest);
			return true;
		} else {
			return false;
		}
	}

}
