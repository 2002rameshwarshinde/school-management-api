package org.dnyanyog.service;

import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.dnyanyog.Dto.Request.FeesRequest;
import org.dnyanyog.Dto.response.FeesResponse;
import org.dnyanyog.Repository.FeesRepository;
import org.dnyanyog.entity.FeesInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public class FeesService {

	@Autowired
	FeesRepository feesRepository;

	@Autowired
	FeesResponse feesResponse;
	@Autowired
	FeesInfo feesinfo;

	public ResponseEntity<FeesResponse> saveData(FeesRequest feesRequest) {

		feesResponse = new FeesResponse();

		
		feesinfo.setGrnNo(feesRequest.getGrnNo());
		feesinfo.setStudentName(feesRequest.getStudentName());
		feesinfo.setAppliedClass(feesRequest.getAppliedClass());
		feesinfo.setTotalFees(feesRequest.getTotalFees());
		feesinfo.setFeesPaid(feesRequest.getFeesPaid());
		feesinfo.setRemainingFess(feesRequest.getRemainingFees());
		feesinfo.setAcadmicYear(feesRequest.getAcadmicYear());

		feesinfo = feesRepository.save(feesinfo);

		feesResponse.setStatus("Success");
		feesResponse.setMessage("Fees Added Successfully");

		return ResponseEntity.status(HttpStatus.CREATED).body(feesResponse);

	}

	public List<FeesInfo> getFeesInfo(long id) {

		return feesRepository.findById(id);

	}

	@Transactional
	public boolean deleteFeesInfoByGrnNo(long grnNo) {
		if (feesRepository.existsByGrnNo(grnNo)) {
			feesRepository.deleteByGrnNo(grnNo);
			return true;
		} else {
			return false;
		}
	}

}
