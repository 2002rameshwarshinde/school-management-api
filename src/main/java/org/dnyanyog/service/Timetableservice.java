package org.dnyanyog.service;

import java.util.List;

import org.dnyanyog.Repository.TimetableRepository;
import org.dnyanyog.entity.Timetable;
import org.springframework.stereotype.Service;

@Service
public class Timetableservice {


    private  TimetableRepository timetableRepository;

    public Timetableservice(TimetableRepository timetableRepository) {
        this.timetableRepository = timetableRepository;
    }

    public List<Timetable> getAllTimetableEntries() {
        return timetableRepository.findAll();
    }

    public List<Timetable> getTimetableEntriesByClass(String className) {
        return timetableRepository.findByClassName(className);
    }

    public Timetable createTimetable(Timetable timetableEntry) {
        
        return timetableRepository.save(timetableEntry);
    }
}

