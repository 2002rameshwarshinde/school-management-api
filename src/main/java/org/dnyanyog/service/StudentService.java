package org.dnyanyog.service;

import org.dnyanyog.Dto.Request.AdmissionRequest;
import org.dnyanyog.Dto.response.AdmissionResponse;
import org.dnyanyog.Dto.response.StudentData;
import org.dnyanyog.Repository.StudentRepository;
import org.dnyanyog.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
@Autowired
AdmissionResponse response;

@Autowired 
Student stud;

@Autowired
StudentRepository studentrepo;

@Autowired
StudentData studentData;

	public ResponseEntity<AdmissionResponse>SaveData(AdmissionRequest request){
		
		response=new AdmissionResponse();
		response.setData(new StudentData());
		
		stud=new Student();
		stud.setStudentName(request.getStudentName());
		stud.setFatherName(request.getFatherName());
		stud.setMotherName(request.getMotherName());
		stud.setLastname(request.getLastname());
         stud.setParentMobileNo(request.getParentMobileNo());
         stud.setAddress(request.getAddress());
         stud.setBirthDate(request.getBirthDate());
         stud.setGender(request.getGender());
         stud.setAppliedClass(request.getAppliedClass());
         stud.setPreviousSchoolName(request.getPreviousSchoolName());
         stud.setStudentMob(request.getStudentMob());
         stud.setEmail(request.getEmail());
         
         stud=studentrepo.save(stud);
        response.setStatus("Sucess");
        response.setMessage("Admission Successful");
        response.getData().setId(stud.getId());
        response.getData().setStudentName(stud.getStudentName());
        response.getData().setFatherName(stud.getFatherName());
        response.getData().setMotherName(stud.getMotherName());
        response.getData().setLastname(stud.getLastname());
        response.getData().setParentMobileNo(stud.getParentMobileNo());
        response.getData().setAddress(stud.getAddress());
        response.getData().setBirthDate(stud.getBirthDate());
        response.getData().setGender(stud.getGender());
        response.getData().setAppliedClass(stud.getAppliedClass());
        response.getData().setPreviousSchoolName(stud.getPreviousSchoolName());
        response.getData().setStudentMob(stud.getStudentMob());
        response.getData().setEmail(stud.getEmail());
        
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
}
