package org.dnyanyog.controller;

import java.util.List;
import org.dnyanyog.Dto.Request.LeaveApplicationRequest;
import org.dnyanyog.entity.LeaveApplication;
import org.dnyanyog.entity.LeaveStatus;
import org.dnyanyog.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class LeaveController {

	@Autowired
	LeaveService leaveApplicationService;

	@Autowired
	LeaveApplication leaveApplication;

	@PostMapping(path = "Leave/api/request")
	public LeaveApplication applyForLeave(@RequestBody LeaveApplicationRequest leaveApplicationRequest) {
		return leaveApplicationService.applyforLeave(leaveApplicationRequest);
	}

	@GetMapping(path = "Leave/api/show/{status}")
	public List<LeaveApplication> getLeaveRequestsByStatus(@PathVariable String status) {
		LeaveStatus leaveStatus = LeaveStatus.valueOf(status.toUpperCase());
		return leaveApplicationService.getLeaveRequestsByStatus(status);
	}

	
	
//	@GetMapping(path="/Leave/api/v1/request/{grnNo}")
//	public ResponseEntity<LeaveApplication> getLeaveRequestByGRN(@PathVariable Long grnNo) {
//	    LeaveApplication leaveRequest = leaveApplicationService.getLeaveRequestByGRN(grnNo);
//
//	    if (leaveRequest != null) {
//	        return ResponseEntity.ok(leaveRequest);
//	    } else {
//	        return ResponseEntity.notFound().build();
//	    }
//	}

	
	
	
	
	
	@PutMapping("/Leave/api/v1/{requestId}/{status}")
	public ResponseEntity<String> updateLeaveRequestStatus(@PathVariable Long requestId,
	                                                       @PathVariable String status) {
	    LeaveStatus leaveStatus;
	    try {
	        leaveStatus = LeaveStatus.valueOf(status.toUpperCase());
	    } catch (IllegalArgumentException e) {
	        return ResponseEntity.badRequest().body("Invalid status provided");
	    }

	    if (leaveStatus == LeaveStatus.APPROVED) {
	        boolean isApproved = leaveApplicationService.approveLeaveRequest(requestId);

	        if (isApproved) {
	            return ResponseEntity.ok("Leave request approved successfully");
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } else if (leaveStatus == LeaveStatus.REJECTED) {
	        boolean isRejected = leaveApplicationService.rejectLeaveRequest(requestId);

	        if (isRejected) {
	            return ResponseEntity.ok("Leave request rejected successfully");
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } else {
	        return ResponseEntity.badRequest().body("Invalid status provided");
	    }
	}

}
